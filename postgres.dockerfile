FROM postgres:12.3

RUN apt update && apt install postgresql-12-postgis-3 -y

RUN  cp /usr/share/zoneinfo/Asia/Tehran /etc/localtime
