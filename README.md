########################## Best Food ###############################

#Run project
- You can use this ip for easy testing of APIs

  - Home 
    - http://130.185.120.205
  
  - Login
    - POST http://130.185.120.205/api/v1/login
    - user : test@gmail.com
    - password : 123456
  
  - Menu (food list)
    - GET http://130.185.120.205/api/v1/menu
  
  - Create Order
    - POST http://130.185.120.205/api/v1/order
    - example : {"food_id":"1"} 

 

########################## best food ###############################



- Or you can run the project manually by following the steps below:
 
  - clone the project 
  - composer install
  - set db environment
  - run migration
  - run seeder
  - run php artisan passport:keys for generate oauth keys
  - login with test user
      - email : test@gmail.com
      - password = 123456



########################## best food ###############################



#APIs (prefix: /api/v1)
- login:
  - POST /login

- food list:
  - GET /menu

- create order:
  - POST /order


