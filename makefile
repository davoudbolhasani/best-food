NAME   := bestfood
TAG    := $$(git log -1 --pretty=%h)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest

php_build:
	docker build -t bestfood-php:1.0.0 -f php.dockerfile .

app_prepare:
	docker build -t bestfood-base:1.0.0 -f bestfood-base.dockerfile .

app_build:
	docker build -t ${IMG} -f bestfood.dockerfile . && \
    	docker tag ${IMG} ${LATEST}

app_up:
	docker-compose -p bestfood up -d

develop_app_up_base:
	BESTFOOD_API_DEBUG=true \
    BESTFOOD_APP_DEBUG=true \
	BESTFOOD_APP_ENV=local \
	BESTFOOD_APP_URL=http://bestfood.local \
	BESTFOOD_API_URL=http://bestfood.local \
    BESTFOOD_DB_DATABASE=postgres \
    BESTFOOD_DB_USERNAME=postgres \
    BESTFOOD_DB_PASSWORD=secret \
    BESTFOOD_REDIS_HOST=redis \
    BESTFOOD_QUEUE_DRIVER=redis \
    BESTFOOD_CACHE_DRIVER=redis \
	docker-compose -p bestfood up -d $(PROJECT)

develop_app_up:
	BESTFOOD_IMAGE_VERSION=${TAG} make develop_app_up_base PROJECT="app"

production_app_up:
	BESTFOOD_IMAGE_VERSION=${TAG} docker-compose -p bestfood up -d app


db_build:
	docker build -t bestfood-postgres:12.3 -f postgres.dockerfile .

db_up:
	docker-compose -p bestfood up -d db

redis_up:
	docker-compose -p bestfood up -d redis

