FROM bestfood-base:1.0.0

ADD . /var/www/

WORKDIR /var/www

RUN chown www-data:www-data -R  /var/www/storage
RUN chown www-data:www-data -R /var/www/bootstrap
RUN chown www-data:www-data -R /var/www/public

RUN chmod 755 ./entrypoint*

ENTRYPOINT ["./entrypoint_fpm.sh"]
