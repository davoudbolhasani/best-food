<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Resources\OrderResource;
use App\Services\Api\V1\Ingredient\IngredientServiceInterface;
use App\Services\Api\V1\Order\OrderServiceInterface;

class OrderController extends Controller
{
    /**
     * @param OrderServiceInterface $service
     * @param IngredientServiceInterface $ingredientService
     */
    public function __construct(protected OrderServiceInterface      $service,
                                protected IngredientServiceInterface $ingredientService)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrderRequest $request
     * @return OrderResource
     */
    public function store(CreateOrderRequest $request): OrderResource
    {
        $order = $this->service->store($request);

        return new OrderResource($order);
    }
}
