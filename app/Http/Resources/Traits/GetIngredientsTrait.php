<?php

namespace App\Http\Resources\Traits;

trait GetIngredientsTrait
{
    public function getIngredients($ingredients)
    {
        $result = [];

        foreach ($ingredients as $ingredient){
            $result[] = [
                'id'          => $ingredient->id,
                'title'       => $ingredient->title,
                'best_before' => $ingredient->best_before,
            ];
        }

        return $result;
    }
}
