<?php

/**
 * Class FoodResource
 *
 * @OA\Schema(
 *     schema="Food",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="title", type="string"),
 *     @OA\Property(property="ingredients", type="array", @OA\Items(ref="#/components/schemas/Ingredient")),
 *     @OA\Property(property="created_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="updated_at", type="string", example="2019-09-11 00:00:00")
 * )
 *
 * @OA\Schema(schema="FoodList", type="array", @OA\Items(ref="#/components/schemas/Food"))
 *
 * @package App\Http\Resources
 */

