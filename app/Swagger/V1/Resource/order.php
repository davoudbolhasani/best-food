<?php

/**
 * Class OrderResource
 *
 * @OA\Schema(
 *     schema="Order",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="food_id", type="integer"),
 *     @OA\Property(property="created_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="updated_at", type="string", example="2019-09-11 00:00:00")
 * )
 *
 * @OA\Schema(schema="Orders", type="array", @OA\Items(ref="#/components/schemas/Order"))
 *
 * @package App\Http\Resources
 */
