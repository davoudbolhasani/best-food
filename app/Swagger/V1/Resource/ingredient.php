<?php

/**
 * Class IngredientResource
 *
 * @OA\Schema(
 *     schema="Ingredient",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="title", type="string"),
 *     @OA\Property(property="best_before", type="date")
 * )
 *
 * @OA\Schema(schema="Ingredients", type="array", @OA\Items(ref="#/components/schemas/Ingredient"))
 *
 * @package App\Http\Resources
 */
