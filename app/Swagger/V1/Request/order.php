<?php

/**
 * Get the validation rules that apply to the request.
 * @OA\Schema(
 *   schema="CreateOrderRequest",
 *   title="Create Order Request",
 *   description="Create Order Request body data",
 *   type="object",
 *   required={"food_id"},
 *   @OA\Property(property="food_id", type="integer")
 *   )
 * @return array
 */

