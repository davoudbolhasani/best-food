<?php


/**
 * Display a listing of the resource.
 *
 * @OA\Get(
 *     path="/api/v1/menu",
 *     tags={"Food"},
 *     operationId="GetAllFood",
 *     description="Get All Food",
 *     @OA\Response(
 *         response=200,
 *         description="All Food response",
 *         @OA\JsonContent(ref="#/components/schemas/Foods")
 *     )
 * )
 */
