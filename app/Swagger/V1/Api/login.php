<?php
/**
 * Create New Order
 *
 * @OA\Post(
 *     path="/api/v1/login",
 *     tags={"Users"},
 *     operationId="Login",
 *     description="Login Api",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(ref="#/components/schemas/LoginRequest")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Order response",
 *         @OA\JsonContent()
 *     )
 * )
 */
