<?php
/**
 * Create New Order
 *
 * @OA\Post(
 *     path="/api/v1/order",
 *     tags={"Orders"},
 *     operationId="CreateOrder",
 *     description="Create Order",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(ref="#/components/schemas/CreateOrderRequest")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Order response",
 *         @OA\JsonContent(ref="#/components/schemas/Order")
 *     )
 * )
 */
