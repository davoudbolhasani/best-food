<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ingredient extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'best_before',
        'expires_at',
        'stock',
    ];

    public function foods(): BelongsToMany
    {
        return $this->belongsToMany(Food::class, 'food_ingredients');
    }
}
