<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Config;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addMinutes(Config::get('app.access-token.expires-in')));
        Passport::refreshTokensExpireIn(Carbon::now()->addMinutes(Config::get('app.access-token.refresh-expires-in')));
        Passport::personalAccessTokensExpireIn(now()->addMonths(Config::get('app.access-token.personal-token-expires-in')));
    }
}
