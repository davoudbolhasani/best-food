<?php

namespace App\Providers;

use App\Services\Api\V1\Food\FoodService;
use App\Services\Api\V1\Food\FoodServiceInterface;
use App\Services\Api\V1\Ingredient\IngredientService;
use App\Services\Api\V1\Ingredient\IngredientServiceInterface;
use App\Services\Api\V1\Order\OrderService;
use App\Services\Api\V1\Order\OrderServiceInterface;
use App\Services\Authentication\AuthenticationService;
use App\Services\Authentication\AuthenticationServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FoodServiceInterface::class, FoodService::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
        $this->app->bind(IngredientServiceInterface::class, IngredientService::class);
        $this->app->bind(AuthenticationServiceInterface::class, AuthenticationService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
