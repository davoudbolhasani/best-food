<?php

namespace App\Providers;

use App\Repositories\CommentRepository\CommentRepository;
use App\Repositories\CommentRepository\CommentRepositoryEloquent;
use App\Repositories\Food\FoodRepository;
use App\Repositories\Food\FoodRepositoryInterface;
use App\Repositories\FoodIngredient\FoodIngredientRepository;
use App\Repositories\FoodIngredient\FoodIngredientRepositoryInterface;
use App\Repositories\Ingredient\IngredientRepository;
use App\Repositories\Ingredient\IngredientRepositoryInterface;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\ProductProviderRepository\ProductProviderRepository;
use App\Repositories\ProductProviderRepository\ProductProviderRepositoryMongoDb;
use App\Repositories\ProductRepository\ProductRepository;
use App\Repositories\ProductRepository\ProductRepositoryEloquent;
use App\Repositories\ProductRepository\ProductRepositoryMongoDb;
use App\Repositories\ProviderRepository\ProviderRepositoryEloquent;
use App\Repositories\ProviderRepository\ProviderRepository;
use App\Repositories\ProviderRepository\ProviderRepositoryMongoDb;
use App\Repositories\VoteRepository\VoteRepository;
use App\Repositories\VoteRepository\VoteRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FoodRepositoryInterface::class, FoodRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(IngredientRepositoryInterface::class, IngredientRepository::class);
        $this->app->bind(FoodIngredientRepositoryInterface::class, FoodIngredientRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
