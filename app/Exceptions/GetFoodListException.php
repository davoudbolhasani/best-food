<?php

namespace App\Exceptions;

use App\Exceptions\Code\CustomErrorCodesTable;
use Symfony\Component\HttpFoundation\Response;

class GetFoodListException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::GET_LIST_FOODS_FAILED['title'];

    public $code = CustomErrorCodesTable::GET_LIST_FOODS_FAILED['code'];
}
