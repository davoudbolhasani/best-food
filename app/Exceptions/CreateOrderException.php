<?php

namespace App\Exceptions;

use App\Exceptions\Code\CustomErrorCodesTable;
use Symfony\Component\HttpFoundation\Response;

class CreateOrderException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::CREATE_ORDER_FAILED['title'];

    public $code = CustomErrorCodesTable::CREATE_ORDER_FAILED['code'];
}
