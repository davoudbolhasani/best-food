<?php

namespace App\Exceptions;

use App\Exceptions\Code\CustomErrorCodesTable;
use Symfony\Component\HttpFoundation\Response;

class LoginFailedException extends ParentException
{
    public int $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = CustomErrorCodesTable::LOGIN_FAILED['title'];

    public $code = CustomErrorCodesTable::LOGIN_FAILED['code'];
}
