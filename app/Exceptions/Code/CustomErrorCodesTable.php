<?php

namespace App\Exceptions\Code;


/**
 * Class CustomErrorCodesTable
 *
 * @author  Johannes Schobel <johannes.schobel@googlemail.com>
 */
class CustomErrorCodesTable
{
    /**
     * Use this class to define your own custom error code tables. Please follow the scheme defined in the other file
     * in order to make them compliant!
     *
     * Please note that Apiato reserves the error codes 000000 - 099999 for itself. If you define your own codes,
     * please start with 100000
     *
     * const BASE_GENERAL_ERROR = [
     *      'code' => 100000,
     *      'title' => 'Unknown / Unspecified Error.',
     *      'description' => 'Something unexpected happened.',
     *  ];
     *
     */
    const LOGIN_FAILED = [
        'code'        => 3000,
        'title'       => 'اطلاعات ورود صحیح نمی باشد',
        'description' => 'An Exception happened during the Login Process.',
    ];

    const GET_LIST_FOODS_FAILED = [
        'code'        => 3001,
        'title'       => 'خطایی رخ داده است',
        'description' => 'get list of foods failed!',
    ];

    const CREATE_ORDER_FAILED = [
        'code'        => 3002,
        'title'       => 'ثبت سفارش با خطا مواجه شد!',
        'description' => 'create order failed!',
    ];
}
