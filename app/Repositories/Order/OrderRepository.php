<?php

namespace App\Repositories\Order;

use App\Models\Order;
use App\Repositories\Repository;
use Prettus\Repository\Criteria\RequestCriteria;
use function app;

class OrderRepository extends Repository implements OrderRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
