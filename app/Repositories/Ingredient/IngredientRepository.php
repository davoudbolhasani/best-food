<?php

namespace App\Repositories\Ingredient;

use App\Models\Ingredient;
use App\Repositories\Repository;
use Prettus\Repository\Criteria\RequestCriteria;
use function app;

class IngredientRepository extends Repository implements IngredientRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Ingredient::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
