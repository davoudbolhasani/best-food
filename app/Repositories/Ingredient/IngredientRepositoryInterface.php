<?php

namespace App\Repositories\Ingredient;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IngredientRepositoryInterface extends RepositoryInterface
{
}
