<?php

namespace App\Services\Api\V1\Order;

use App\Exceptions\CreateOrderException;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Services\Api\V1\Food\FoodServiceInterface;
use App\Services\Api\V1\Ingredient\IngredientServiceInterface;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class OrderService implements OrderServiceInterface
{
    /**
     * @var OrderRepositoryInterface
     * @var IngredientServiceInterface
     * @var FoodServiceInterface
     */
    public function __construct(protected OrderRepositoryInterface   $orderRepository,
                                protected IngredientServiceInterface $ingredientService,
                                protected FoodServiceInterface       $foodService)
    {

    }

    /**
     * @param $request
     * @return mixed
     */
    public function store($request): mixed
    {
        try {
            DB::beginTransaction();
            $userId = $request->user()->id;
            $data = [
                'user_id' => $userId,
                'food_id' => $request->food_id,
            ];

            $order = $this->orderRepository->create($data);
            $food = $this->foodService->find($request->food_id);
            $ingredients = $food->ingredients->pluck('id')->toArray();

            $this->ingredientService->reduceStock($ingredients, Config::get('ingredient.decrement_count'));

            DB::commit();
            return $order;
        } catch (Exception $error) {
            DB::rollBack();
            throw new CreateOrderException();
        }
    }
}
