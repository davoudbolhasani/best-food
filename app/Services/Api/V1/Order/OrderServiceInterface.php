<?php

namespace App\Services\Api\V1\Order;

interface OrderServiceInterface
{
    public function store($request);
}
