<?php

namespace App\Services\Api\V1\Food;

interface FoodServiceInterface
{
    public function index($request);

    public function find(int $id);
}
