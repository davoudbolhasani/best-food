<?php

namespace App\Services\Api\V1\Food;

use App\Exceptions\GetFoodListException;
use App\Repositories\Food\FoodRepositoryInterface;
use Exception;

class FoodService implements FoodServiceInterface
{
    /**
     * @var FoodRepositoryInterface
     */
    public function __construct(protected FoodRepositoryInterface $repository)
    {

    }

    /**
     * @param $request
     * @return mixed
     * @throws Exception
     */
    public function index($request): mixed
    {
        try {
            $foods = $this->repository->with('ingredients')
                ->withMin('ingredients', 'best_before')
                ->whereDoesntHave('ingredients',
                    function ($query) {
                        $query->where('stock', '<=', 0)
                            ->orWhere('expires_at', '<', date('Y-m-d'));
                    })->orderBy('ingredients_min_best_before','desc')->paginate();
        } catch (Exception $error) {
            throw new GetFoodListException();
        }

        return $foods;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id): mixed
    {
        return $this->repository->with('ingredients')->find($id);
    }
}
