<?php

namespace App\Services\Api\V1\Ingredient;

interface IngredientServiceInterface
{
    public function reduceStock($ingredientIds, $count);

    public function increaseStock($ingredientIds, $count);
}
