<?php

namespace App\Services\Api\V1\Ingredient;

use App\Repositories\Ingredient\IngredientRepositoryInterface;

class IngredientService implements IngredientServiceInterface
{
    /**
     * @var IngredientRepositoryInterface
     */
    public function __construct(protected IngredientRepositoryInterface $ingredientRepository,)
    {

    }

    /**
     * @param $ingredientIds
     * @param $count
     * @return mixed
     */
    public function reduceStock($ingredientIds, $count): mixed
    {
        return $this->ingredientRepository->whereIn('id', $ingredientIds)
            ->decrement('stock', $count);
    }

    /**
     * @param $ingredientIds
     * @param $count
     * @return void
     */
    public function increaseStock($ingredientIds, $count)
    {
        return $this->ingredientRepository->whereIn('id', $ingredientIds)
            ->increment('stock', $count);
    }
}
