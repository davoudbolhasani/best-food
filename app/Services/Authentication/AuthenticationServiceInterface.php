<?php

namespace App\Services\Authentication;

interface AuthenticationServiceInterface
{
    public function login($request);

    public function callOAuth($data);
}
