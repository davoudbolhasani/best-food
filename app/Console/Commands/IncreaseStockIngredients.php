<?php

namespace App\Console\Commands;

use App\Repositories\Ingredient\IngredientRepositoryInterface;
use App\Services\Api\V1\Ingredient\IngredientServiceInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class IncreaseStockIngredients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingredient:increase-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'increase stock of finished ingredients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(protected IngredientRepositoryInterface $repository,
                                protected IngredientServiceInterface $service)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $ingredientIds = $this->repository->where('stock', 0)->where('expires_at', '>=', date('Y-m-d'))->pluck('id');

        $this->service->increaseStock($ingredientIds, Config::get('ingredient.increment_count'));

        echo $this->info('done');
    }
}
