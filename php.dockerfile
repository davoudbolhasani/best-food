FROM php:8.0-fpm-alpine

MAINTAINER David Bolhasani <davidbolhasani@gmail.com>

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/community' >> /etc/apk/repositories
RUN apk update && \
	apk add --no-cache 'tidyhtml-dev==5.2.0-r1'

RUN apk add  --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.6/main/ nodejs yarn

RUN apk add  --update \
	git \
	unzip \
	openssl \
	g++ \
	libtool \
	make \
	curl \
	autoconf \
	libldap \
	tidyhtml \
	openldap-dev \
	libmemcached-dev \
	freetype-dev \
	oniguruma-dev \
	libjpeg-turbo-dev \
	libxml2-dev \
	libpng \
    libzip \
    libzip-dev\
	libpng-dev \
	libmcrypt-dev \
	postgresql-dev \
	zlib-dev \
	icu-dev \
	libmcrypt-dev \
    libwebp \
    libwebp-dev \
	tzdata \
	nano

RUN  cp /usr/share/zoneinfo/Asia/Tehran /etc/localtime \
     && echo "Asia/Tehran" >  /etc/timezone \
     && echo "Asia/Tehran" >  /etc/TZ

RUN docker-php-ext-install  mysqli pdo soap xml tokenizer exif pdo_pgsql intl pdo_mysql iconv tidy zip  mbstring ldap bcmath

RUN docker-php-ext-configure gd --with-freetype --with-webp --with-jpeg && \
    docker-php-ext-install gd

RUN cd /usr && curl -sS https://getcomposer.org/installer | php -- --version=2.1.0 && \
	 mv /usr/composer.phar /usr/local/bin/composer

RUN pecl install -o -f redis && \
    echo "extension=redis.so" > /usr/local/etc/php/conf.d/docker-php-ext-redis.ini && \
    rm -rf /tmp/pear
