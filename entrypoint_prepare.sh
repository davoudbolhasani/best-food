#!/usr/bin/env bash

echo "==> Repair environment variables"
cp /var/www/.env.example /var/www/.env

echo "==> Start to clear cached data"
php /var/www/artisan config:clear
#php /var/www/artisan cache:clear
php /var/www/artisan route:clear
php /var/www/artisan view:clear
php /var/www/artisan clear-compiled
echo "==> Cached cleared successfully"


echo "==> Start to generate documents"

#php artisan l5-swagger:generate
php artisan l5-swagger:generate
echo "==> Complete Generate documents"


echo "==> Start to reset permission resets"
php /var/www/artisan permission:cache-reset
echo "==> Reset Cache has been complete"

echo "==> Start to set permissions"
chmod 777 /var/www/bootstrap/cache
chmod 777 -R storage
echo "==> Permission set successfully"


if [ "$APP_ENV" = "local" ] || [ "$APP_ENV" = "staging" ]
then
    echo "==> Start to procced public and private key"
    cp /var/www/storage/test-assets/* /var/www/storage
    echo "==> public and private key copy has done"

    echo "==> Start to run migrations"
    php artisan migrate --force
    echo "==> Complete migrations"
fi

php /var/www/artisan optimize
php /var/www/artisan config:cache
php /var/www/artisan event:cache
php /var/www/artisan route:cache
php /var/www/artisan view:cache


