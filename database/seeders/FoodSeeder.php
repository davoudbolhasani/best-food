<?php

namespace Database\Seeders;

use App\Models\Food;
use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents('database/seeders/foods.json');
        $data = json_decode($json, true)['recipes'];

        foreach ($data as $datum) {
            $foodData = [
                'title' => $datum['title'],
            ];

            $food = Food::create($foodData);
            $ingredientsId = Ingredient::whereIn('title', $datum['ingredients'])->pluck('id');

            $food->ingredients()->attach($ingredientsId);
        }
    }

}
