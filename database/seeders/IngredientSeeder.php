<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents('database/seeders/ingredients.json');
        $data = json_decode($json, true)['ingredients'];

        foreach ($data as $datum) {
            Ingredient::create($datum);
        }
    }

}
