<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_ingredients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('food_id')->constrained('foods')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('ingredient_id')->constrained('ingredients')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->unique(['food_id', 'ingredient_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_ingredients');
    }
};
