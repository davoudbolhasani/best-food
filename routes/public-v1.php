<?php

use Illuminate\Support\Facades\Route;

Route::post('login', 'AuthController@loginUsingPasswordGrant')->name('login');
