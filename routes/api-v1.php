<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('menu', 'FoodController')->only(['index']);
Route::apiResource('order', 'OrderController')->only(['store']);
