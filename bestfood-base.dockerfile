FROM bestfood-php:1.0.0

MAINTAINER David Bolhasani <davidbolhasani@gmail.com>

ADD . /var/www/

WORKDIR /var/www

COPY php-upload.www.ini /usr/local/etc/php/conf.d/bestfood-upload.ini
COPY php-fpm.www.ini /usr/local/etc/php-fpm.d/www.conf

RUN composer install --optimize-autoloader --no-dev --no-progress
